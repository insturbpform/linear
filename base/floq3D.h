!      ---------------------------------------------------
!      parameters for arnos3d
       integer Ny,Nz,NzCos,NzSin,NtotCos,NtotSin,Ntot
       integer n
       double precision ymax
       parameter (ymax=2.0)
       parameter (Ny=512,Nz=16)
       parameter (n=Ny)
       parameter (NzCos=Nz+1,NzSin=Nz-1)
       parameter (NtotCos=(1+Ny)*NzCos,NtotSin=(1+Ny)*NzSin)
       parameter (Ntot=NtotCos+NtotSin)
!      ---------------------------------------------------

