!
! $Id: chaCIturb.f,v 1.3 2007/01/29 09:18:59 greg Exp greg $
! $Source: /home/greg/Documents/CHANNEL_POISEUILLE/turbulent_Greg/chaCIturb.f,v $
!
c     
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     
c     program computes the least few least stable modes of a 
c     turbulent channel flow
c     temporal orr-sommerfeld solver using collocation and
c     routines from netlib 
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
! $Log: chaCIturb.f,v $
! Revision 1.3  2007/01/29 09:18:59  greg
! Last version of chaCIturb.exe
! Validation thanks to
! Butler & Farrell 1993
! Del Alamo & Jimenez 2006
!
! Revision 1.2  2007/01/11 08:58:44  greg
! new version using "implicit none"
!
! Revision 1.1  2007/01/11 08:46:49  greg
! Initial revision
!
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      program chaCIturb
      implicit none
c
      integer nmax,nma2
      include 'floq3D.h'
      parameter(nmax=Ny+1,nma2=2*nmax)
*      parameter(nmax=250,nma2=2*nmax)
c
      integer i,j,k,imax,nm,ni,nm1,nparfil,np1,iy
      integer ifail,ijob,info,ider
      real*8 ar(0:2*Ny+1,0:2*Ny+1),ai(0:2*Ny+1,0:2*Ny+1)
      real*8 b(0:2*Ny+1,0:2*Ny+1)
      real*8 ar1(0:Ny,0:Ny),ai1(0:Ny,0:Ny),b1(0:Ny,0:Ny)
      real*8 ar2(0:Ny,0:Ny),ai2(0:Ny,0:Ny),b2(0:Ny,0:Ny)
      real*8 c(0:Ny,0:Ny)
      real*8 t(0:Ny,0:Ny,0:4)
c*******************************************************************
c     derivation et integration matrices      
      real*8 D0(0:Ny,0:Ny),D1(0:Ny,0:Ny),D2(0:Ny,0:Ny)
      real*8 D(0:Ny,0:Ny),DD(0:Ny,0:Ny)
      real*8 D0inv(0:Ny,0:Ny)
      real*8 ExtD(0:Ny-1,0:Ny-1),ExtDinv(0:Ny-1,0:Ny-1)
      real*8 column(0:Ny),columnp(0:Ny-1)
      real*8 Extup(0:Ny-1),Extu(0:Ny-1)
      integer intep(0:Ny)
      integer intepp(0:Ny-1)
c*******************************************************************      
      real*8 u(0:Ny),up(0:Ny),upp(0:Ny),eta(0:Ny)
      real*8 fr(1:2*(Ny+1),1:2*(Ny+1)),fi(1:2*(Ny+1),1:2*(Ny+1))
      real*8 wk(1:2*(Ny+1))
      real*8 zr(1:2*(Ny+1),1:2*(Ny+1)),zi(1:2*(Ny+1),1:2*(Ny+1))
      real*8 cr(1:2*(Ny+1)),ci(1:2*(Ny+1)),cimax
      real*8 alpha,beta,kk2,re,pi,sca,s,sgn
c*******************************************************************
c     new parameters : in definition of the eddy viscosity according to
c     Del Alamo&Jimenez : kk=0.426 aa=25.4
c     Butler&Farrell : kk=0.525 aa=37 
      real*8 nu,aa,kk,h
      integer rep 
      parameter(nu=1.55e-5,h=1.0d0)
      real*8 nut(0:Ny),nutp(0:Ny),nutpp(0:Ny),nutppp(0:Ny)
c*******************************************************************
      real*8 toto
      integer inte(1:2*(Ny+1))
      real*8 wint(0:Ny)
c
      complex*16 zero,one,iota
      complex*16 velin(0:Ny),vrtin(0:Ny)
      complex*16 uloc(0:Ny),vloc(0:Ny),wloc(0:Ny)
      complex*16 uvwmode(1:3*(Ny+1))
      real*8 weight(1:3*(Ny+1))
!
c     input parameters
      open(unit=2,file='para2DCHA.h',status='unknown')
      rewind 2
      read (2,*) nparfil
      read (2,*) alpha
      write(*,*) 'alpha =',alpha
      read (2,*) beta
      write(*,*) 'beta =',beta
      read (2,*) re
      write(*,*) 'Re =',re
      close(2)
c      write(*,*) '**************************************************'
c      write(*,*) 'Reynolds & Tiederman Profile : parameters A and K'
c      write(*,*) '1 --> Del Alamo & Jimenez 2006'
c      write(*,*) '2 --> Butler & Farrell 1992'
c      read(*,*) rep
c      if (rep.eq.1) then
!!!!!!!!!!!!!! these values are from del Alamo & Jimenez
       aa = 25.4d0
       kk=0.426d0
c      elseif (rep.eq.2) then
c       aa = 37.0d0
c       kk=0.525d0
c       aa=89.0d0
c       kk=0.995d0
c      endif
c      write(*,*) 'A =',aa
c      write(*,*) 'K =',kk
c      write(*,*) '**************************************************'
!
c     set all parameters
c
      if (n.gt.nmax) stop 'nmax exceeded'
      zero=(0.d0,0.d0)
      one =(1.d0,0.d0)
      iota=(0.d0,1.d0)
      pi  =4.d0*datan(1.d0)
      kk2 = alpha*alpha + beta*beta
c     
c     set up Chebyshev polynomials 
c
      do k=0,n
         do j=0,n
            t(j,k,0)=cos(j*k*pi/n)
         end do
      end do

c
c     set up derivatives of Chebyshev polynomials 
c
      do ider=1,4
         do j=0,n
            t(j,0,ider)=0.
            t(j,1,ider)=t(j,0,ider-1)
            t(j,2,ider)=4.*t(j,1,ider-1)
            do k=3,n
               t(j,k,ider)=2.*k*t(j,k-1,ider-1)+k*t(j,k-2,ider)/(k-2)
            end do
         end do
      end do
   
c     set up the grid
      do j=0,Ny
       eta(j)=cos(j*pi/Ny)
      enddo
c
c*******************************************************************
c     set up the eddy viscosity according to 
c     Cess analytic approximation (1958)
c
c     nut = 0.5*nu*sqrt(1+kk²*re²/9*(1.-eta²)²*
c           (1.+2*eta²)²*(1-exp(-(1.-|eta|)*re/aa))²)-
c           0.5*nu
!     THIS IS THE TURBULENT VISCOSITY ALONE
      do j=0,Ny
	 nut(j)=0.5d0*sqrt(1.d0+(kk*re/3.d0
     &     *(1.d0-eta(j)**2)
     &     *(1.d0+2.d0*eta(j)**2)
     &     *(1.d0-exp(-(1.d0-abs(eta(j)))*re/aa)))**2)-0.5d0
      end do
!
c*******************************************************************


c*******************************************************************
c     compute mean profile and derivatives
c
!     the formula below is ok (p.4 of Waleffe & Kim)
!     and considers the full viscosity
      do j=0,Ny
         up(j)=-eta(j)*re/(nut(j)+1.d0)
      end do
c
c      Setting up derivation matrice in physical space
c      D=t(.,.,1)*inv(t(.,.,0))=D1*D0inv
c      DD=t(.,.,2)*inv(t(.,.,0))=D2*D0inv
c*******************************************************************
c STEP 1  :  Inversion of t(.,.,0)=D0
      do j=0,n
	 do k=0,n
	   D0(j,k)=t(j,k,0)
	   D1(j,k)=t(j,k,1)
           D2(j,k)=t(j,k,2)
	 end do
      end do

!     initialize
      do j=0,Ny
         intep(j)=0
      end do
      np1=Ny+1
!     factorize
      info=0
      call dgefa(D0,np1,np1,intep,info)
      if (info.ne.0) write(*,*) info,' inv1 - failed'
      do j=0,Ny
         do i=0,Ny
            column(i)=0.d0
         end do
         column(j)=1.d0
         call dgesl(D0,np1,np1,intep,column(0),0)
         do i=0,Ny
            D0inv(i,j)=column(i)
         end do
      end do

      write(*,*) 'INVERSION D0..........................[ OK ]'

c*******************************************************************
c  STEP 2  :  Compute the product D1*D0inv

      do i=0,Ny
         do j=0,Ny
c	    D(i,j)=0.d0
	    do k=0,Ny
	    D(i,j)=D(i,j)+D1(i,k)*D0inv(k,j)
	    DD(i,j)=DD(i,j)+D2(i,k)*D0inv(k,j)
	    end do
	 end do
      end do

      write(*,*) 'CALCUL D=D1*D0inv.....................[ OK ]'
c*******************************************************************
c  STEP 3  :  Compute the second derivative of velocity U"=D*U'

      do j=0,Ny
	 upp(j)=0.d0
	 do k=0,Ny
	 upp(j)=upp(j)+D(j,k)*up(k)
	 end do
      end do
c
      write(*,*) 'CALCUL U"=D*Up........................[ OK ]'
c*******************************************************************
c  STEP 4  :  Extraction of inversible part of D and inversion

      nm1=Ny-1
      do i=0,nm1
         do j=0,nm1
	 ExtD(i,j)=D(i+1,j+1)
	 end do
      end do

      do j=0,nm1
         intepp(j)=0
      end do
      info=0
      call dgefa(ExtD,Ny,Ny,intepp,info)
      if (info.ne.0) write(*,*) info,' inv1 - failed'
      do j=0,nm1
         do i=0,nm1
            columnp(i)=0.d0
         end do
         columnp(j)=1.d0
         call dgesl(ExtD,Ny,Ny,intepp,columnp(0),0)
         do i=0,nm1
            ExtDinv(i,j)=columnp(i)
         end do
      end do

      write(*,*) 'EXTRACTION PARTIE INVERSIBLE D........[ OK ]'
c*******************************************************************
c  STEP 5  :  Extraction of the corresponding part of U'
c             and compute the integration

      do j=1,Ny
	 Extup(j-1)=up(j)
      end do

      do j=0,nm1
c	 Extu(j)=0.
	 do k=0,nm1
	 Extu(j)=Extu(j)+ExtDinv(j,k)*Extup(k)
	 end do
      end do

      write(*,*) 'INTEGRATION VECTEUR EXTRAIT...........[ OK ]'
c*******************************************************************
c  STEP 6  :  Computing the mean velocity profile

      do j=0,Ny
         u(j)=0.d0
      end do
      do j=1,Ny
	 u(j)=Extu(j-1)
      end do
c     just in case...
      u(Ny)=0.d0
c-test- test-test-test-test-test-test-test-test-test-test-test-test-
      open(unit=23,file='velocity.dat')
      rewind(23)
      do k=0,Ny
       write(23,999) eta(k),eta(k)*re,0.d0,u(k),up(k),upp(k)
     &              ,0.d0,0.d0
      enddo
      close(23)

* 999  format(100(1X,E16.10))

c-test- test-test-test-test-test-test-test-test-test-test-test-test-

      write(*,*) 'CALCUL U..............................[ OK ]'
c*******************************************************************
c     compute integration weight
c
      do iy=0,n
         wint(iy)=-.5
         do ni=0,(n+2)/2-3
            wint(iy) = wint(iy) + cos(pi*float(2*(ni+1)*iy)/float(n))/
     +           float((2*(ni+1))**2-1)
         enddo
         wint(iy) = wint(iy) + .5*cos(pi*float(iy))/float(n**2-1)
         wint(iy) = -4./float(n)*wint(iy)
         if(iy.eq.0.or.iy.eq.n) wint(iy)=wint(iy)*.5
      enddo
c
c     calculate Orr-Sommerfeld-Squire matrices
c
c*******************************************************************
c      do j=0,n
c         do k=0,n
c            ar1(j,k)=-(u(j)*t(j,k,2)-kk2*u(j)*t(j,k,0)
c     +                 -upp(j)*t(j,k,0))*alpha
c            ai1(j,k)=-(t(j,k,4)-2.*kk2*t(j,k,2)+kk2*kk2*t(j,k,0))/re
c            b1(j,k)=-t(j,k,2)+kk2*t(j,k,0)
c
c            ar2(j,k)=u(j)*t(j,k,0)*alpha
c            ai2(j,k)=(t(j,k,2)-kk2*t(j,k,0))/re
c            b2(j,k)=t(j,k,0)
c
c            c(j,k)=beta*up(j)*t(j,k,0)
c         enddo
c      enddo
c*******************************************************************
c en pr�ision, calcul des d�iv�s premi�e et seconde de nuT. 
      do j=0,Ny
         nutp(j)=0.d0
         do k=0,n
	    nutp(j)=nutp(j)+D(j,k)*nut(k)
         end do
      end do
c
      do j=0,n
         nutpp(j)=0.d0
         do k=0,n
             nutpp(j)=nutpp(j)+DD(j,k)*nut(k)
         end do
      end do
! c
! 
! 
      open(unit=17,file='viscosity.dat')
      rewind(17)
      do i=0,n
       write(17,999) eta(i),eta(i)*re,0.d0,0.d0
     &              ,(nut(i)+1.d0),nutp(i),nutpp(i)
      end do
      close(17)
c
!      
 999  format(100(1X,E16.10))
!
      stop
      end
c     #################################################################
      real*8 function sgn(x)
      real*8 x
      if (x.gt.0.0) then
       sgn=1.0
      else if (x.lt.0.0) then
       sgn=-1.0
      else if (x.eq.0.0) then
       sgn=0.0
      else 
       write(*,*) 'error in function sgn'
       stop
      end if
      return
      end
c     #################################################################
      include 'support.f'
c     #################################################################



