c      **************************************************************
!      $Id: BetaLoop.f,v 1.3 2008/02/13 12:14:25 carlo Exp carlo $
!      $Source: /home/carlo/W2/PSA/VALIDATION/SRC/RCS/BetaLoop.f,v $
!
!
!      **************************************************************
       program BetaLoop
!       
       implicit none
       integer Ny,Mtrunc,Nb,i,Nlog
       double precision Re,alpha,beta,lamzmax,lamzmin,dlamz,pi
       double precision lamz,lamzlog
!
!      define wall normal grid and truncation
       open(unit=2,file='loop.i')
       rewind(2)
       read(2,*)
       read(2,*)
       read(2,*) Ny,Mtrunc
       read(2,*) 
       read(2,*) Re,alpha
       read(2,*) 
       read(2,*) lamzmin,lamzmax
       read(2,*) 
       read(2,*) Nb,Nlog
       close(2)
          
       ! define pi   
       pi=4.d0*datan(1.d0)
       
       dlamz=(lamzmax-lamzmin)/Nb
       
       if (Nlog .eq. 1) then
       dlamz=0.
       dlamz=(log10(lamzmax)-log10(lamzmin))/Nb
       end if
c---------------------------------------------------------------------

       ! Construct parameter file
        call system('rm -f para2DCHA.h para2D.h')
!
       open(unit=2,file='para2D.h')
       rewind(2)
        write(2,*) Ny
        write(2,*) alpha
        write(2,*) beta
        write(2,*) Re
       close(2)
            
       call system('cp para2D.h para2DCHA.h')
       
       ! Construct Chebyshev differentiation operators
       call system('matlab -nosplash -nodisplay -r GenDmat')
          
       ! Write main parameters such Re, alpha. 
       write(*,*) 'Re= ',Re
       write(*,*) 'alpha= ',alpha
       
       ! Creat GmaxBeta.dat
c       call system('mkdir results')
       call system('rm -f Gmax/*.dat')
       call system('touch Gmax/GmaxBeta.dat')
       
       ! Creat RmaxBeta.dat
       call system('rm -f Rmax/*.dat')
       call system('touch Rmax/RmaxBeta.dat')
       
       ! Start Running Loop
       
       do i=0,Nb
       	
       	lamz=lamzmin+i*dlamz
       	
       	if (Nlog .eq. 1) then
       	lamzlog=log10(lamzmin)+i*dlamz
        lamz=10.**lamzlog
        end if
      
        beta=2.*pi/lamz
        
        write(*,*) '================================================'
        write(*,*) 'beta= ',beta,'lambdaz=',lamz
        
!       run codes
        call OneRun(Ny,Re,alpha,beta,i)
       end do
       
       ! Delete D*.dat, eigenvalues and eigenvectors.
       call system('rm -f D*.dat eigvec.dat eigval.dat')
       call system('rm -f Gmax.dat Rmax.dat')
       call system('rm -f crci.dat opt*.* ')
!
       stop 
       end
!      ***************************************************************
       subroutine OneRun(Ny,Re,alpha,beta,iLoop)
       implicit none
       integer Ny,Nt,iLoop
       double precision Re,alpha,beta,ymax,ki,ueplus,tmax
     &                 ,pi,lambdazstar,lambdaz99,delta99,beta99
     &                 ,ymaxmax,lambdazplus,toto,jack
!

!      constants
       pi=4.d0*datan(1.d0)
!
       Nt=1000
       
       write(*,*) 'tmax=',tmax
       
       call system('rm -f para2DCHA.h para2D.h')
!
       open(unit=2,file='para2D.h')
       rewind(2)
        write(2,*) Ny
        write(2,*) alpha
        write(2,*) beta
        write(2,*) Re
       close(2)
       
        
       
       call system('cp para2D.h para2DCHA.h')
       
c       open(unit=2,file='nn.ipt')
c        rewind(2)
c        write(2,*) tmax
c        write(2,*) tmax/float(Nt)
c       close(2)

       call RunCodes
       
       if (iLoop.le. 99) then
           call StoreResults(iLoop)
       end if	
!
       return
       end
!      ***************************************************************
       subroutine RunCodes
       implicit none
!
       call system('./chaCI_Turb_DM_ifort.exe')
       call system('./nnshCHA2D_ifort.exe')
       call system('cat Gmax.dat')
       call system('cat Gmax.dat >> Gmax/GmaxBeta.dat')
       call system('cat Rmax.dat')
       call system('cat Rmax.dat >> Rmax/RmaxBeta.dat')

       return
       end

!     ***************************************************************
      subroutine StoreResults(i)
      implicit none
      integer i
      character*58 jak
      character*2  joe
!
      jak='010203040506070809101112131415161718192021222324252627282930'
      joe=jak((2*i+1):(2*i+2))
      
      call system('mv nrmExp.dat nrmExp'//joe//'.dat')
      call system('mv nrmExp'//joe//'.dat ./Gmax')
      call system('mv optMod.dat optMod'//joe//'.dat')
      call system('mv optMod'//joe//'.dat ./Gmax')
      call system('mv optRes.dat optRes'//joe//'.dat')
      call system('mv optRes'//joe//'.dat ./Gmax')
      
      call system('mv nrmRes.dat nrmRes'//joe//'.dat')
      call system('mv nrmRes'//joe//'.dat ./Rmax')
      call system('mv optf.dat optf'//joe//'.dat')
      call system('mv optf'//joe//'.dat ./Rmax')
      call system('mv optfRes.dat optfRes'//joe//'.dat')
      call system('mv optfRes'//joe//'.dat ./Rmax')

      return
      end
