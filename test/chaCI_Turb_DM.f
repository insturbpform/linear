! $Id: chaCI_Turb_DM.f,v 1.2 2008/02/10 21:50:39 carlo Exp carlo $
! $Source: /moogwai/usr/carlo/bubu/WORK/VALIDATION/SRC/RCS/chaCI_Turb_DM.f,v $
!
c     
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c     
c     program computes the least few least stable modes of a 
c     channel flow
c     temporal orr-sommerfeld solver using collocation and
c     routines from netlib 
c
cccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccccc
c
      program chaCI
c
      include 'floq3Dold.h'
c
      integer i,j,imax
      real*8 ar(1:2*(Ny-1),1:2*(Ny-1)),ai(1:2*(Ny-1),1:2*(Ny-1))
      real*8 b(1:2*(Ny-1),1:2*(Ny-1))
      real*8 ar1(1:Ny-1,1:Ny-1),ai1(1:Ny-1,1:Ny-1),b1(1:Ny-1,1:Ny-1)
      real*8 ar2(1:Ny-1,1:Ny-1),ai2(1:Ny-1,1:Ny-1),b2(1:Ny-1,1:Ny-1)
      real*8 c(1:Ny-1,1:Ny-1)
      real*8 u(0:Ny),up(0:Ny),upp(0:Ny),eta(0:Ny)
      real*8 nutot(0:Ny),nutotp(0:Ny),nutotpp(0:Ny)
      real*8 fr(1:2*(Ny-1),1:2*(Ny-1)),fi(1:2*(Ny-1),1:2*(Ny-1))
      real*8 wk(1:2*(Ny-1))
      real*8 zr(1:2*(Ny-1),1:2*(Ny-1)),zi(1:2*(Ny-1),1:2*(Ny-1))
      real*8 cr(1:2*(Ny-1)),ci(1:2*(Ny-1))
      real*8 alpha,beta,kk2,re,pi,sca,rescale
      real*8 toto,jack
      integer inte(1:2*(Ny-1))
      real*8 wint(0:Ny)
      real*8 DM(1:Ny-1,1:Ny-1,0:4)
c
      complex*16 zero,one,iota
      complex*16 velin(1:Ny-1),vrtin(1:Ny-1)
      complex*16 uloc(0:Ny),vloc(0:Ny),wloc(0:Ny)
      complex*16 uvwmode(1:3*(Ny+1))
      real*8 weight(1:3*(Ny+1))
!
!
c     input parameters
      open(unit=2,file='para2DCHA.h',status='unknown')
      rewind 2
      read (2,*) n
      read (2,*) alpha
      read (2,*) beta
      read (2,*) re
      close(2)
!
      write(*,*) n,alpha,beta,re
!
      if (n.ne.Ny) then
       write(*,*) 'n not equal to Ny: stopping'
       print*,n,Ny
       stop
      end if 
!
      call dmread(n,DM) 
c     set all parameters
      zero=(0.d0,0.d0)
      one =(1.d0,0.d0)
      iota=(0.d0,1.d0)
      pi=4.d0*datan(1.d0)
      kk2 = alpha*alpha + beta*beta
c     
c     set up the grid
      do j=0,n
       eta(j)=cos(j*pi/n)
      enddo
!
      open(unit=17,file='velocity.dat')
      rewind(17)
      do j=0,n
       read(17,999) jack,jack,jack,u(j)
     &            ,up(j),upp(j)
     &            ,jack,jack
       u(j)=1.d0-eta(j)**2.d0 
       up(j)=-2.d0*eta(j)
       upp(j)=-2.d0
      end do
      close(17) 
c
      open(unit=18,file='viscosity.dat')
      rewind(18)
      do j=0,n
       read(18,999) jack,jack,jack,jack
     &            ,nutot(j),nutotp(j),nutotpp(j)
       nutot(j)=1.d0
       nutotp(j)=0.d0
       nutotpp(j)=0.d0
      end do
      close(18)
!
 999  format(100(1X,E16.10))
c
      do iy=0,n
         wint(iy)=-.5
         do ni=0,(n+2)/2-3
            wint(iy) = wint(iy) + cos(pi*float(2*(ni+1)*iy)/float(n))/
     +           float((2*(ni+1))**2-1)
         enddo
         wint(iy) = wint(iy) + .5*cos(pi*float(iy))/float(n**2-1)
         wint(iy) = -4./float(n)*wint(iy)
         if(iy.eq.0.or.iy.eq.n) wint(iy)=wint(iy)*.5
      enddo
c
c     
c     calculate Orr-Sommerfeld-Squire matrices
c
      do j=1,n-1
         do k=1,n-1
            ar1(j,k)=-(u(j)*DM(j,k,2)-kk2*u(j)*DM(j,k,0)
     &                 -upp(j)*DM(j,k,0))*alpha
            ai1(j,k)=-(1.d0/re)*(
     &	     nutot(j)*(DM(j,k,4)-2.d0*kk2*DM(j,k,2)+kk2*kk2*DM(j,k,0))
     &	    +2.*nutotp(j)*(DM(j,k,3)-kk2*DM(j,k,1))
     &	    +nutotpp(j)*(DM(j,k,2)+kk2*DM(j,k,0))
     &      )
            b1(j,k)=-(DM(j,k,2)-kk2*DM(j,k,0))
            ar2(j,k)=u(j)*DM(j,k,0)*alpha
            ai2(j,k)=(1.d0/re)*(
     &	     nutot(j)*(DM(j,k,2)-kk2*DM(j,k,0))
     &	    +nutotp(j)*DM(j,k,1)
     &	    )
            b2(j,k)=DM(j,k,0)
            c(j,k)=beta*up(j)*DM(j,k,0)
         enddo
      enddo

c
c     patch matrices together into one big matrix 
c
      do j=1,n-1
         do k=1,n-1
            ar(j    ,k    ) = ar1(j,k) 
            ar(j+n-1,k+n-1) = ar2(j,k)
            ar(j+n-1,k    ) = c(j,k) 
            ar(j    ,k+n-1) = 0.0 
            ai(j    ,k    ) = ai1(j,k) 
            ai(j+n-1,k+n-1) = ai2(j,k) 
            ai(j    ,k+n-1) = 0.0 
            ai(j+n-1,k    ) = 0.0 
            b(j    ,k)     = b1(j,k) 
            b(j+n-1,k+n-1) = b2(j,k)
            b(j    ,k+n-1) = 0.0
            b(j+n-1,k    ) = 0.0
         enddo
      enddo
c
c     invert the B-matrix and multiply it to the A-matrix 
c
      nm=2*(n-1)
      info=0
      call dgefa(b,nm,nm,inte,info)
      if (info.ne.0) write(*,*) info,' inv1 - failed'
      do i=1,nm
         call dgesl(b,nm,nm,inte,ar(1,i),0)
         call dgesl(b,nm,nm,inte,ai(1,i),0)
      enddo

c
c     solve for the eigenvalues/vectors (if ijob nonzero) of invB*A 
      ijob = 1
      call cg(nm,nm,ar,ai,cr,ci,ijob,zr,zi,fr,fi,wk,ifail)
      if (ifail.ne.0) write(*,*) ifail,' eig1 - failed'
c
c
c    
c     sort eigenvalues and eigenvectors according to ci
      do k=1,nm
       cimax=ci(k)
       imax=k
       do i=k,nm
        if (ci(i).gt.cimax) then
         cimax=ci(i)
         imax=i
        end if
       end do
       toto=cr(k)
       cr(k)=cr(imax)
       cr(imax)=toto
       toto=ci(k)
       ci(k)=ci(imax)
       ci(imax)=toto
       do j=1,nm
        toto=zr(j,k)
        zr(j,k)=zr(j,imax)
        zr(j,imax)=toto
        toto=zi(j,k)
        zi(j,k)=zi(j,imax)
        zi(j,imax)=toto
       end do
      end do

!     write out cr,ci
      open(unit=20,file='crci.dat',status='unknown')
      rewind(20)
      do i=1,nm
       write(20,*) cr(i)/alpha,ci(i)/alpha
      enddo
      close(20)
!      
       open(unit=2,file='rescale.h')
        rewind(2)
        read(2,*) rescale
       close(2)
!     write out eigenvalues in lambda form      
      open(unit=20,file='eigval.dat',status='unknown')
      rewind(20)
      do k=1,nm
       write(20,*) ci(k)*rescale,-cr(k)*rescale
      enddo
      close(20)
c
c     write out mode in physical space in u,v,w form 
!     loop in mode index k
      open(unit=21,file='eigvec.dat')
      rewind(21)
      do k=1,nm
!      specify mode amplitudes in chebyshev space
       do i=1,n-1
	velin(i)=one*zr(i    ,k)+iota*zi(i    ,k)
        vrtin(i)=one*zr(i+n-1,k)+iota*zi(i+n-1,k)
       end do
!      go back in y-phys space with all the components
       do j=1,n-1
!       just go back in y-phys space with v
        vloc(j)=velin(j)
        uloc(j)=zero
        wloc(j)=zero
        do i=1,n-1
!        compute u = (i/k^2)(alpha D v - beta eta)
         uloc(j)=uloc(j)+(iota/kk2)*
     &	       (alpha*DM(j,i,1)*velin(i)-beta*DM(j,i,0)*vrtin(i))
!        compute w = (i/k^2)(beta D v + alpha eta)
         wloc(j)=wloc(j)+(iota/kk2)*
     &	       (beta*DM(j,i,1)*velin(i)+alpha*DM(j,i,0)*vrtin(i))
        end do
       end do     
       uloc(0)=zero
       vloc(0)=zero
       wloc(0)=zero
       uloc(n)=zero
       vloc(n)=zero
       wloc(n)=zero
       do j=0,n
        uvwmode(j+1+0*(n+1))=uloc(j)
        uvwmode(j+1+1*(n+1))=vloc(j)
        uvwmode(j+1+2*(n+1))=wloc(j)
       end do
       write(21,*) (uvwmode(j),j=1,3*(n+1))
      end do
      close(21)
!      
!     writeout the weight matrix
      do j=0,n
       weight(j+1+0*(n+1))=wint(j)
       weight(j+1+1*(n+1))=wint(j)
       weight(j+1+2*(n+1))=wint(j)
      end do
      open(unit=21,file='weight.dat')
      rewind(21)
       write(21,*) (weight(j),j=1,3*(n+1))
      close(21) 
!     writeout the geometry
      open(unit=21,file='eta.dat')
      rewind(21)
      do j=0,n
       write(21,*) (1.d0+eta(j))/rescale
      end do 
      close(21) 
!
      stop
      end
!     ***************************************************************      
      subroutine dmread(n,DM) 
!
      implicit none
      integer n,i,j,k       
      character*6 dpfil(1:4)
      real*8 DM(1:n-1,1:n-1,0:4)
!
      dpfil(1)='D1.dat'
      dpfil(2)='D2.dat'
      dpfil(3)='D3.dat'
      dpfil(4)='D4.dat'
!
      do j=1,n-1
       do i=1,n-1
        DM(i,j,0)=0.0
       end do
       DM(j,j,0)=1.0
      end do 	

      do k=1,4
       write(*,*) 'loading: ',dpfil(k)
       open(unit=17,file=dpfil(k))
       rewind(17)
       do i=1,n-1
        read(17,*) (DM(i,j,k),j=1,n-1)
       end do
       close(17)
      end do 	

*      write(*,*) DM(2,2,0)

!
* 998  format(1000(E1.16,1X))

!
      return
      end         
c     #################################################################
      include 'support.f'
c     #################################################################
