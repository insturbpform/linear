!      ---------------------------------------------------
!      parameters for arnos3d
       integer Ny,Nz,NzCos,NzSin,NtotCos,NtotSin,Ntot
       parameter (Ny=128,Nz=16)
       parameter (NzCos=Nz+1,NzSin=Nz-1)
       parameter (NtotCos=(1+Ny)*NzCos,NtotSin=(1+Ny)*NzSin)
       parameter (Ntot=NtotCos+NtotSin)
!      ---------------------------------------------------
