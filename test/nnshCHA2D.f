c
c
c RCS IDENTIFICATION
c $Id: nnshCHA2D.f,v 1.2 2008/02/10 21:51:07 carlo Exp carlo $
c $Source: /home/carlo/W2/PSA/VALIDATION/SRC/RCS/nnshCHA2D.f,v $c
c
c      DESCRIPTION
c      Evaluate non-normality in 2D channel flow
c      and compute the optimal perturbations leading to the optimal
c      growth at the final time t_f
c
c      AUTHOR
c      Carlo Cossu, LadHyX (2003)
c      carlo.cossu@ladhyx.polytechnique.fr
c
c
c-----------------------------------------------------------------------
c 
c      The subroutine for computing resolvent norm along the real line
c      is added to compute optimal harmonic response: 
c      See subroutine spseudo real.
c
c                                Modified at 22/10/2008
c                                    LadHyX, Ecole Polytechnique,
c                                                  Yongyun Hwang.
c-----------------------------------------------------------------------
c                                        
c
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c              %                                 %
c              %         MAIN PROGRAM            %
c              %                                 %
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c
c
c
c      ********************************************************
       program nnshtg
c
       call sbegmesg
       call sinmat
       call sbuildwt
       call snrmexp
c      call spseudo
       call spseudoreal
c
       stop
       end
c
c
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c              %                                 %
c              %        CORE SUBROUTINES         %
c              %                                 %
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c
c
c
c
c      ********************************************************
       subroutine sbegmesg
c
c
       write(*,*) '***************************************'
       write(*,*) '$Revision: 1.2 $'
       write(*,*) '$Date: 2008/02/10 21:51:07 $'
       write(*,*) '$State: Exp $'
       write(*,*) '$Author: carlo $'
       write(*,*) '$Locker: carlo $'
       write(*,*) '***************************************'
c
       return
       end
c      ********************************************************
       subroutine sinmat
c
c      input of cleaned eigenvalues, right and left
c      eigenvectors from files
c
c
       implicit none

       include 'floq3D.h'
       integer m
       include 'trunc.h'
c
       integer nn
       parameter(nn=3*(n+1))
c
       integer i,j,llunit,mmunit
       real*8 b1,b2,weight
       complex*16  vrloc(1:nn)
       complex*16  ev
       common/aev/ev(1:m)
       complex*16  vr
       common/avr/vr(1:nn,1:m)
       common/ametric/weight(1:nn)
c
c
       write(*,*) 'nn=',nn
       write(*,*) 'm=',m
c
       open(unit=25,file='eigval.dat')
       rewind(25)
       do i=1,m
        read(25,*) b1,b2
        ev(i)=(1.,0.)*b1+(0.,1.)*b2
       end do
       close(25)
c
       open(unit=25,file='eigvec.dat')
       rewind(25)
       do i=1,m
        read(25,*) (vrloc(j),j=1,nn)
c        write(*,*) i, ' EVi=',ev(i)
        do j=1,nn
         vr(j,i)=vrloc(j)
c         write(*,*) 'vr(',j,',',i,') =',vr(j,i)
 	end do
       end do
       close(25)
c
       open(unit=25,file='weight.dat')
       rewind(25)
	read(25,*) (weight(j),j=1,nn)
       close(25)
c
*       open (unit=17,FILE='ev.dat')
*       rewind(17)
*       do i=1,m
*        write(17,*) ev(i)
*       end do
*       close(17)
c      
       return
       end
c      ********************************************************
       subroutine sbuildwt
c
c      build W~ matrix
c
c       
       implicit none
c
       integer m       
       include 'floq3D.h'
       include 'trunc.h'
c
       integer nn
       parameter(nn=3*(n+1))
c
       integer i,k,h
       real*8 weight
       complex*16 v1dotv2,wt,wti
       complex*16 v1(1:nn)
       complex*16 v2(1:nn)
       common/ametric/weight(1:nn)
       common/awti/wti(1:m,1:m)
       common/aweight/wt(1:m,1:m)
       complex*16  ev
       common/aev/ev(1:m)
       complex*16  vr
       common/avr/vr(1:nn,1:m)
c
       complex*16 zdotc
       external zdotc
c       
       do k=1,m
        do h=1,m
	 do i=1,nn
	  v1(i)=vr(i,k)
	  v2(i)=weight(i)*vr(i,h)
*          write(*,*) 'i=',i
	 end do
*         write(*,*) h,k
	 v1dotv2=zdotc(nn,v1,1,v2,1)
	 wt(k,h)=v1dotv2
	end do
       end do
c
       do h=1,m
        do k=1,m
         wti(k,h)=wt(k,h)
        end do
       end do
c
       call smatinvZ(wti)
c
       return
       end
c     ********************************************************
       subroutine soutoptmod(optmodT,iout)
c       
       implicit none
c
       integer m,iout,icomp       
       include 'floq3D.h'
       include 'trunc.h'
c
       integer nn
       parameter(nn=3*(n+1))
c
       integer i,k
       complex*16 v1dotv2,c
       complex*16 v1(1:m)
       complex*16 v2(1:m)
       complex*16 optModT(1:m)
       complex*16 optMod(1:nn)
       complex*16  vr
       common/avr/vr(1:nn,1:m)
       real*8 eta(0:n)
c
       complex*16 zdotu
       external zdotu
c       
!      build back mode from amplitude to physycal space
       do i=1,nn
        optMod(i)=(0.0d0,0.0d0)
        do k=1,m
	       v1(k)=vr(i,k)
         v2(k)=optmodT(k)
        end do
        	v1dotv2=zdotu(m,v1,1,v2,1)
	        optMod(i)=v1dotv2
       end do
!      
!      rescale the optimal IC with the v maximum
!      and the optimal response with the u maximum
       if (iout.eq.1) then
        icomp=1 		! v
       else
        icomp=0 		! u
       endif	
       c=(0.d0,0.d0)
       do i=1,n+1
        if (abs(optMod(i+1+icomp*(n+1))).gt.abs(c)) then
	 c=optMod(i+1+icomp*(n+1))
	end if
       end do  
!       c=1.
       write(*,*) 'c =',c
       do i=1,nn
	 optMod(i)=optMod(i)/c
       end do
c
       if (iout.eq.1) then
        open(unit=23,file='optMod.dat')
        open(unit=24,file='optModT.dat')
       else if (iout.eq.2) then
        open(unit=23,file='optRes.dat')
        open(unit=24,file='optResT.dat')
       else
        write(*,*) 'Wrong iout in outMod routine:',iout
        write(*,*) 'Stopping the program'
	stop
       end if	
       rewind(23)
       rewind(24)
!       
       do k=1,m
	write(24,*) k,dble(optModT(k)),dimag(optModT(k))
       end do
       close(24)
!
!     read geometry
      open(unit=21,file='eta.dat')
      rewind(21)
      do i=0,n
       read(21,*) eta(i)
      end do 
      close(21) 
      do i=0,n
       write(23,999) eta(i)
     & ,dble(optMod(i+1+0*(n+1))),dimag(optMod(i+1+0*(n+1)))
     & ,dble(optMod(i+1+1*(n+1))),dimag(optMod(i+1+1*(n+1)))
     & ,dble(optMod(i+1+2*(n+1))),dimag(optMod(i+1+2*(n+1)))
*       write(23,*) eta(i)
*     & ,dble(optMod(i+1+0*(Ny+1)))
*     & ,dble(optMod(i+1+1*(Ny+1)))
*     & ,dble(optMod(i+1+2*(Ny+1)))
       end do
       close(23)
c
!
  999  format(30(1X,E16.10))
!
       return
       end
c     ********************************************************
       subroutine soutoptmodIC(optmodT)
c       
       implicit none
c
       integer m
       include 'floq3D.h'
       include 'trunc.h'
c
       integer nn
       parameter(nn=3*(n+1))
c
       integer i,k
       complex*16 v1dotv2,c
       complex*16 v1(1:m)
       complex*16 v2(1:m)
       complex*16 optModT(1:m)
       complex*16 optMod(1:nn)
       complex*16  vr
       common/avr/vr(1:nn,1:m)
       real*8 eta(0:n)
       real*8 weight
       common/ametric/weight(1:nn)
c
       complex*16 zdotu
       external zdotu
c       
!      here rebuild the velocity field in the non-reduced space
       do i=1,nn
        do k=1,m
	 v1(k)=vr(i,k)
         v2(k)=optmodT(k)
        end do
	v1dotv2=zdotu(m,v1,1,v2,1)
	optMod(i)=v1dotv2
       end do
!       
!      
!      rescale the optimal i.c. with the v center-channel amplitude
!      so that you have a real signal for V and U and imaginary for W
       i=n/2
       c=optMod(i+1+1*(n+1))
!       c=1.
       do i=1,nn
        optMod(i)=optMod(i)/c
       end do
!      compute the norm of the optimal mode and then
!      normalize it to 1
       v1dotv2=(0.,0.)
       do i=1,nn
	      v1dotv2=v1dotv2+optMod(i)*weight(i)*conjg(optMod(i))
       end do
       write(*,*) v1dotv2
       do i=1,nn
        optMod(i)=optMod(i)/sqrt(zabs(v1dotv2))
       end do
c
       open(unit=23,file='optIC.u')
       rewind(23)
       do i=0,n
       write(23,*)
     &  optMod(i+1+0*(n+1))
     & ,optMod(i+1+1*(n+1))
     & ,optMod(i+1+2*(n+1))
       end do
       close(23)
!
*  999  format(30(1X,E16.10))
!
       return
       end
c
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c              %                                 %
c              %     COMPUTE EXPONENTIAL NORM    %
c              %                                 %
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c
c
c
c     ********************************************************
       subroutine snrmexp
c       
       implicit none
c
       integer m
       include 'trunc.h'
c
       integer i,j
       real*8 normExp,t,tmax,dt,tMEA,tt,rep
       complex*16 optModT(1:m)
       complex*16 optModTMEA(1:m)
       complex*16 optResTMEA(1:m)
       complex*16 optResT(1:m)
       real*8 normExpold
       real*8 nn,alpha,beta,re,ymax,nnn,lamz
       real*8 pi
       
      pi=4.d0*datan(1.d0)
c      
       open(unit=17,file='nn.ipt')
       rewind(17)
        read(17,*) tmax
        read(17,*) dt
       close(17)

      open(unit=2,file='para2D.h',status='unknown')
      rewind 2
      read (2,*) nnn
      read (2,*) alpha
      read (2,*) beta
      read (2,*) re
      
      close(2)
c
c      initialize
c
       lamz=2.*pi/beta
       
       normExp=1.
c      
       open(unit=17,file='nrmExp.dat')
       rewind(17)
c       
       normExpold=0.d0
       tMEA=0.
       do t=dt,tmax,dt
        call siniopt(optModT)
        call snormHapw(t,normExp,optModT,OptResT)
*        call snormHaev(t,normExp,optModT)

	      write(17,*) t,normExp
c      finding the optimal perturbation giving the best amplification
        if (normExp.ge.normExpold) then
        	
        do i=1,m
           optModTMEA(i)=optModT(i)
        enddo
        
        tMEA=t
        normExpold=normExp
        
c        write(*,*) 'time for maximum energy amplification =',tMEA
c        write(*,*) 'Emax =',normExpold 

        end if
        if (normExp.lt.0.001) then
         goto 650
        endif
       end do

650    close(17)
       open(unit=50,file='Gmax.dat')
       rewind(50)
*      write(50,999) re,alpha,beta,normExpold,tMEA
       write(50,999) beta,normExpold**2,normExpold,lamz,tMEA,re
       close(50)
c
!      output of optimal mode
       call sOutOptMod(optModTMEA,1)

!      output of optimal response
       call timeevo(tMEA,optModTMEA,optResTMEA)
       call sOutOptMod(optResTMEA,2)

  999  format(30(1X,E16.10))
!
c      writing time development of optModMEA
c       t=0.d0
c       call timeevo(t,optModTMEA,optResTMEA)
c       call soutoptmodevo(optResTMEA,0)
c       write(*,*) 'Write time evolution for optimal perturbation?'
c       write(*,*) '1 ------> Yes'
c       write(*,*) 'else ---> No'
c       read(*,*) rep
c       if (rep.eq.1) then
c       j=1
c       do t=0.0d0,tmax,tmax/500 
c        call timeevo(t,optModTMEA,optResTMEA)
c        call soutoptmodevo(optResTMEA,j)
c        tt=j*tMEA/10.+j/2.
c        call timeevo(tt,optModTMEA,optResTMEA)
c        call soutoptmodevo(optResTMEA,j+10 )
c        j=j+1
c       enddo
c       t=tMEA
c       call timeevo(t,optModTMEA,optResTMEA)
c       call soutoptmodevo(optResTMEA,2)
c       t=tmax
c       call timeevo(t,optModTMEA,optResTMEA)
c       call soutoptmodevo(optResTMEA,3)
!
!      debug code
*       call sOutOptModTest(optModT,1)
*       call sOutOptModTest(optResT,2)
!
c       endif
c        enddo
       return
       end
c     ********************************************************
      subroutine snormHapw(t,anormout,mode2,optres)
!
!     Here we use power iterations to compute the energy
!     norm in the truncated space.
!
!     t: 	input, time at which the optimum is computed
!     anormout: L2 norm (sqrt of the 2*energy)
!     mode 2:	on input guess for optimal mode
!		on output, the optimal mode in reduced space
! 
!
!
c     ----------------------------------------------------
      implicit none
c
      integer m
      include 'trunc.h'
c
      integer i,j,niter,nitmax
      complex*16 zone,zzero
      complex*16 anorm,weight
      complex*16 mode2(1:m)
      complex*16 optres(1:m)
      complex*16 mode2new(1:m)
      complex*16 work1(1:m)
      complex*16 work2(1:m)
      real*8 t,epsilon,anormout
      complex*16 anormnew,normmod,temp,resid
c
      complex*16 wt,wti
      common/aweight/wt(1:m,1:m)
      common/awti/wti(1:m,1:m)
      complex*16  ev
      common/aev/ev(1:m)
c
      complex*16 zdotc
      external zdotc,zgemv
c     -----------------------------------------------------
      zzero=(0.d0,0.d0)
      zone=(1.d0,0.d0)
c
      anorm=zone*anormout**2
      epsilon=1.0e-5
      nitmax=1500
      niter=0
c
 1    niter=niter+1
c
c     w1 = [A] mode2
      do i=1,m
       work1(i)=zexp(ev(i)*t)*mode2(i)
      end do
c     w2 = [W~] w1 = [W~ A] mode2
      call zgemv('N',m,m,zone,wt,m,work1,1,zzero,work2,1)
c     w1 = [A^H] w2 = [A^H W~ A] mode2
      do i=1,m
       work1(i)=conjg(zexp(ev(i)*t))*work2(i)
      end do
c     mode2new = [W~]^(-1) w1 =[W~]^(-1) [A^H W~ A] mode2
      call zgemv('N',m,m,zone,wti,m,work1,1,zzero,mode2new,1)
c
c     normmod = mode2^H mode2new =  mode2^H [A^H W~ A] mode2
      normmod=zdotc(m,mode2,1,mode2new,1)
c
c     temp = mode2^H mode2     
      temp=   zdotc(m,mode2,1,mode2,1)
c
c
c     anormnew = mode2^H mode2new / mode2^H [W~] mode2 =
c                mode2^H [A^H W~ A] mode2 / mode2^H [W~] mode2
c     and at convergence [A^H W~ A] mode2 = anorm^2 [W~] mode2 => 
c     anormnew --> anorm^2
      anormnew=normmod/temp
c
c     normalize so that mode2new^H mode2new = 1
c     normmod = mode2new^H mode2new
      normmod=zdotc(m,mode2new,1,mode2new,1)
      
      do j=1,m
       mode2new(j)=mode2new(j)/zsqrt(normmod)
      end do
c
c     convergence evaluated on the norm of A
      resid=(zsqrt(anormnew)/zsqrt(anorm)-(1.d0,0.d0))
!       write(*,*) niter,zabs(resid)
c
      anorm=anormnew
      do j=1,m
       mode2(j)=mode2new(j)
      end do
c
      if ((zabs(resid).gt.epsilon).and.(niter.lt.nitmax)) goto 1
      if (niter.ge.nitmax) write(*,*) '****** WARNING:MAX ITER ******'
*     write(*,*) niter,resid,anorm
      anormout=dsqrt(dble(anorm))
      
c     w1 = [A] mode2
      do i=1,m
       optres(i)=zexp(ev(i)*t)*mode2(i)
      end do
c     -----------------------------------------------------
      return
      end
c     ********************************************************
      subroutine snormHaev(t,anormout,mode2)
c     ----------------------------------------------------
      implicit none
c
*      include 'param.h'
*      integer n
*      parameter(n=N2)
      integer m
      include 'trunc.h'
      integer lwork
      parameter(lwork=m**2)
c
      integer i,j,info
      complex*16 anorm,weight
      complex*16 mode2(1:m)
      real*8 t,anormout
c
      complex*16 php(1:m,1:m)
      complex*16 b(1:m,1:m)
      complex*16 work(1:lwork)
      real*8 rwork(1:3*m-2)
      real*8 w(1:m)
      complex*16 wt
      common/aweight/wt(1:m,1:m)
      complex*16  ev
      common/aev/ev(1:m)
c
      complex*16 zdotc
      external zdotc,zgemv
c     -----------------------------------------------------
c     PHPtemp = [A^H W~ A]
      do j=1,m
       do i=1,m
	php(i,j)=conjg(zexp(ev(i)*t))*wt(i,j)*zexp(ev(j)*t) 
       end do
      end do
c
      do j=1,m
       do i=1,m
	b(i,j)=wt(i,j)
       end do
      end do 
c
      call ZHEGV(1,'N','U',m,php,m,b,m,W,WORK,LWORK,RWORK,INFO)
      if (info.ne.0) write(*,*) '****** WARNING INFO=',info
c
*      do j=1,m
*       write(*,*) j,w(j)
*      end do 
c
      anormout=dsqrt(w(m))      
c     -----------------------------------------------------
      return
      end
c
c
c
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c              %                                 %
c              %     COMPUTE RESOLVENT NORM      %
c              %                                 %
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c
c
c
c
c     ********************************************************
      subroutine spseudo
c       
      implicit none
c
      integer m
      include 'trunc.h'
c
      integer i,j,nzr,nzi
      real*8 normRes,zrmin,zrmax,zimin,zimax,zr,zi
      complex*16 z
      complex*16 optModZ(1:m)
c      
c      
      open(unit=17,file='pseudospe.ipt')
      rewind(17)
       read(17,*) zrmin
       read(17,*) zrmax
       read(17,*) zimin
       read(17,*) zimax
       read(17,*) nzr
       read(17,*) nzi
      close(17)
c
      open(unit=17,file='nrmRes.dat')
      open(unit=18,file='nrmResGnu.dat')
      rewind(17)
      rewind(18)
c       
      normRes=1.
c      
      call siniopt(optModZ)
c
      do i=0,nzr
       zr=zrmin+(zrmax-zrmin)*dble(i)/dble(nzr)
       do j=0,nzi
        zi=zimin+(zimax-zimin)*dble(j)/dble(nzi)
	z=(1.d0,0.d0)*zr+(0.d0,1.d0)*zi 
c	 
        call snormZapw(z,normRes,optModZ)
*        call snormZaev(z,normRes,optModZ)
c	 
c	write(*,*) z,normRes
	write(17,*) zr,zi,normRes
	write(18,*) zr,zi,normRes
       end do
       write(18,1000)
      end do
c
      close(17)
      close(18)
c       
 1000 format()
c
      return
      end


c=======================================================================
c=======================================================================      
c-----------------------------------------------------------------------
c  The subroutine 'spseudoreal' is a modified version of 'spseudo'
c  for evaluating optimal response to harmonic forcing. The purpose
c  of this subroutine is to obtain maximal response with respect to
c  the change in forcing frequency, the profile of optimal forcing and 
c  response.
c                    
c                              Modified at 22/10/2008
c                                    LadHyX, Ecole Polytechnique.
c                                                  Yongyun Hwang.
c-----------------------------------------------------------------------

      subroutine spseudoreal
c       
      implicit none
c
      integer m
      include 'trunc.h'
c
      integer i,j,nzi
      real*8 normRes,zimin,zimax,zi
      real*8 nnn, alpha, beta, re, lamz
      real*8 pi
      complex*16 z
      complex*16 optModZ(1:m),optResZ(1:m)
      real*8 zifmax,normResmax
      complex*16 optModZmax(1:m),optResZmax(1:m)
       
      pi=acos(0.)*2.
      
      open(unit=2,file='para2D.h',status='unknown')
      rewind 2
      read (2,*) nnn
      read (2,*) alpha
      read (2,*) beta
      read (2,*) re
      close(2)
      
      lamz=2.*pi/beta
      
      open(unit=17,file='harmonic.ipt')
      rewind(17)
       read(17,*)
       read(17,*)
       read(17,*) zimin,zimax,nzi
      close(17)
      
      print *,'zimin =',zimin
      print *,'zimax =',zimax
      print *,'nzi=',nzi
     
c
      open(unit=17,file='nrmRes.dat')
      rewind(17)
c       
       normRes=1.
       normResmax=0.

c------- Start of zi loop for obtaining maximal response ---------------  
    
      do i=0,nzi     	
       zi=zimin+(zimax-zimin)*dble(i)/dble(nzi)
	     z=(0.d0,1.d0)*zi 
	     
	     ! Put initial guess as randon perturbations.           
	      call siniopt(optModZ)   
	     
	     ! This is added for the branch swiching. 
	      optModZ(1)=cmplx(5.,3.)
	      optModZ(2)=cmplx(0.,0.)
	      
	      write(*,*) optModZ(1),optModZ(2)                                               
                                                               
c      ! obtain the optimal response to harmonic forcing.
c       - optModz : input - initial guess,
c                   output - optimal forcing profile in projected space.
c       - optResZ : input - default
c                   output - optimal response in projected space.              
c       - normRes : Amplitude of maximum response.
c       - z       : forcinf frequency in the complex domain.

        call snormZapw(z,normRes,optModZ,optResZ)
        
c      ! obtain the maximum response with forcing frequency zi       
        if (normRes .GE. normResmax) then
        	zifmax=zi
        	normResmax=normRes
        do j=1,m
        	optmodzmax(j)=optmodz(j)
        	optreszmax(j)=optresz(j)
        end do   
        end if
 
c      ! write the trace of normRes with respect to forcing frequency zi
	      write(17,999) zi,normRes,lamz,beta,re
	    
      end do
      
c------  End of zi loop  -----------------------------------------------
      
      ! obtain maximum response
      open(unit=18,file='Rmax.dat')   
      write(18,999) beta, normResmax**2., normResmax, lamz,
     &  zifmax, abs(zifmax), re
      
      ! output of optimal mode
      call sOutOptres(optModzmax,1)

      ! output of optimal mode
      call sOutOptres(optreszmax,2)
      
      close(18)
      close(17)
 999  format(30(1X,E16.10))
c
      return
      end
      
c-----------------------------------------------------------------------
c      Subroutine soutoptres is a modefied version of subroutine sout
c      optmode to export profile of optimal harmonic foring and its
c      response. 
c-----------------------------------------------------------------------
       subroutine soutoptres(optmodT,iout)

       implicit none
c
       integer m,iout,icomp       
       include 'floq3D.h'
       include 'trunc.h'
c
       integer nn
       parameter(nn=3*(n+1))
c
       integer i,k
       complex*16 v1dotv2,c
       complex*16 v1(1:m)
       complex*16 v2(1:m)
       complex*16 optModT(1:m)
       complex*16 optMod(1:nn)
       complex*16  vr
       common/avr/vr(1:nn,1:m)
       real*8 eta(0:n)
c
       complex*16 zdotu
       external zdotu
c       
!      build back mode from amplitude to physycal space
       do i=1,nn
        optMod(i)=(0.0d0,0.0d0)
        do k=1,m
	       v1(k)=vr(i,k)
         v2(k)=optmodT(k)
        end do
        	v1dotv2=zdotu(m,v1,1,v2,1)
	        optMod(i)=v1dotv2
       end do
!      
!      rescale the optimal IC with the v maximum
!      and the optimal response with the u maximum
       if (iout.eq.1) then
        icomp=1 		! v
       else
        icomp=0 		! u
       endif	
       c=(0.d0,0.d0)
       do i=1,n+1
        if (abs(optMod(i+1+icomp*(n+1))).gt.abs(c)) then
	       c=optMod(i+1+icomp*(n+1))
       	end if
!       	c=1.
       end do  
       write(*,*) 'c_R =',c
       do i=1,nn
	       optMod(i)=optMod(i)/c
       end do
c
       if (iout.eq.1) then
        open(unit=23,file='optf.dat')
        open(unit=24,file='optfT.dat')
       else if (iout.eq.2) then
        open(unit=23,file='optfRes.dat')
        open(unit=24,file='optfResT.dat')
       else
        write(*,*) 'Wrong iout in outMod routine:',iout
        write(*,*) 'Stopping the program'
       	stop
       end if	
       rewind(23)
       rewind(24)
!       
       do k=1,m
      	write(24,*) k,dble(optModT(k)),dimag(optModT(k))
       end do
       close(24)
!
!     read geometry
      open(unit=21,file='eta.dat')
      rewind(21)
      do i=0,n
       read(21,*) eta(i)
      end do 
      close(21) 
      do i=0,n
       write(23,999) eta(i)
     & ,dble(optMod(i+1+0*(n+1))),dimag(optMod(i+1+0*(n+1)))
     & ,dble(optMod(i+1+1*(n+1))),dimag(optMod(i+1+1*(n+1)))
     & ,dble(optMod(i+1+2*(n+1))),dimag(optMod(i+1+2*(n+1)))

       end do
       close(23)
c
!
  999  format(30(1X,E16.10))
!
       return
       end
      
c=======================================================================
c=======================================================================      
      
c     ********************************************************
      subroutine snormZapw(z,anormout,mode2,Res)
c     ----------------------------------------------------
      implicit none
c
      integer m
      include 'trunc.h'
c
      integer i,j,niter,nitmax
      complex*16 zone,zzero
      complex*16 anorm,weight
      complex*16 mode2(1:m)
      complex*16 mode2new(1:m)
      complex*16 work1(1:m)
      complex*16 work2(1:m)
      complex*16 Res(1:m)
      real*8 epsilon,anormout
      complex*16 anormnew,normmod,temp,resid,z
c
      complex*16 wt,wti,ev
      common/aweight/wt(1:m,1:m)
      common/awti/wti(1:m,1:m)
      common/aev/ev(1:m)
c
      complex*16 zdotc
      external zdotc,zgemv
c     -----------------------------------------------------
      zzero=(0.d0,0.d0)
      zone=(1.d0,0.d0)
c
      anorm=zone*anormout**2
      epsilon=1.0e-15
      nitmax=1000
      niter=0
c
 1    niter=niter+1
c
c     w1 = [A] mode2
      do i=1,m
       work1(i)=(zone/(z-ev(i)))*mode2(i)
      end do 
c     w2 = [W~] w1 = [W~ A] mode2
      call zgemv('N',m,m,zone,wt,m,work1,1,zzero,work2,1)
c     w1 = [A^H] w2 = [A^H W~ A] mode2
      do i=1,m
       work1(i)=conjg(zone/(z-ev(i)))*work2(i)
      end do 
c     mode2new = [W~]^(-1) w1 =[W~]^(-1) [A^H W~ A] mode2
      call zgemv('N',m,m,zone,wti,m,work1,1,zzero,mode2new,1)
c
c     normmod = mode2^H mode2new =  mode2^H [A^H W~ A] mode2
      normmod=zdotc(m,mode2,1,mode2new,1)
c
c     temp = mode2^H mode2     
      temp=   zdotc(m,mode2,1,mode2,1)
c
c
c     anormnew = mode2^H mode2new / mode2^H [W~] mode2 =
c                mode2^H [A^H W~ A] mode2 / mode2^H [W~] mode2
c     and at convergence [A^H W~ A] mode2 = anorm^2 [W~] mode2 => 
c     anormnew --> anorm^2
      anormnew=normmod/temp
c
c     normalize so that mode2new^H mode2new = 1
c     normmod = mode2new^H mode2new 
      normmod=zdotc(m,mode2new,1,mode2new,1)
      do j=1,m
       mode2new(j)=mode2new(j)/zsqrt(normmod)
      end do
c
c     convergence evaluated on the norm of A
      resid=(zsqrt(anormnew)/zsqrt(anorm)-(1.d0,0.d0))
*     write(*,*) niter,resid
c
      anorm=anormnew
      do j=1,m
       mode2(j)=mode2new(j)
      end do
c
      if ((zabs(resid).gt.epsilon).and.(niter.lt.nitmax)) goto 1
      if (niter.ge.nitmax) write(*,*) '****** WARNING:MAX ITER ******'
c      write(*,*) niter,resid,anorm
      anormout=dsqrt(dble(anorm))
      
      do i=1,m
       res(i)=(zone/(z-ev(i)))*mode2(i)
      end do
      
c     -----------------------------------------------------
      return
      end
c     ********************************************************
      subroutine snormZaev(z,anormout,mode2)
c     ----------------------------------------------------
      implicit none
c
      integer m
      include 'trunc.h'
      integer lwork
      parameter(lwork=m**2)
c
      integer i,j,info
      complex*16 anorm,weight
      complex*16 mode2(1:m)
      complex*16 z,zone
      real*8 anormout
c
      complex*16 wt,ev
      complex*16 php(1:m,1:m)
      complex*16 b(1:m,1:m)
      complex*16 work(1:lwork)
      real*8 rwork(1:3*m-2)
      real*8 w(1:m)
      common/aweight/wt(1:m,1:m)
      common/aev/ev(1:m)
c
      complex*16 zdotc
      external zdotc,zgemv
c     -----------------------------------------------------
c     constants definition
      zone=(1.d0,0.d0)
c
c     PHPtemp = [A^H W~ A]
      do j=1,m
       do i=1,m
	php(i,j)=conjg(zone/(z-ev(i)))*wt(i,j)*(zone/(z-ev(j)))
       end do
      end do 
c
      do j=1,m
       do i=1,m
	b(i,j)=wt(i,j)
       end do
      end do 
c
      call ZHEGV(1,'N','U',m,php,m,b,m,W,WORK,LWORK,RWORK,INFO)
      if (info.ne.0) write(*,*) '****** WARNING INFO=',info
c
*      do j=1,m
*       write(*,*) j,w(j)
*      end do 
c      
      anormout=dsqrt(w(m))      
c     -----------------------------------------------------
      return
      end
c
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c              %                                 %
c              %         SERVICE ROUTINES        %
c              %                                 %
c              %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
c
c
c     ****************************************************
      subroutine smatinvZ(a)
c     ----------------------------------------------------
      implicit none
c
      integer m
      include 'trunc.h'
      integer lwork
      parameter(lwork=m*m)
c
      integer info,i,j,nrhs,lda,ldb
c
      complex*16 a(1:m,1:m)
      complex*16 bb(1:m,1:m)
      complex*16 work(1:lwork)
      integer ipiv(1:m)
c
      external zhesv
c
      nrhs=m
      lda=m
      ldb=m
      info=0
c
      do j=1,m
       do i=1,m
        bb(i,j)=(0.d0,0.d0)
       end do
       bb(j,j)=(1.d0,0.d0)
      end do
c
      call zhesv('U',m,nrhs,a,lda,ipiv,bb,ldb,work,lwork,info)
c
      if (info.ne.0) then
       write (*,*) 'SLINSL LAPACK VER. FAIL; INFO=',info
      end if
c
      do j=1,m
       do i=1,m
        a(i,j)=bb(i,j)
       end do
      end do
c     ----------------------------------------------------
      return
      end
c     ********************************************************
       subroutine siniopt(optModZ)
c
       implicit none
c
       integer m
       include 'trunc.h'
c
       integer i
       integer iseed(1:4)
       complex*16 optModZ(1:m)
       complex*16 zlarnd
c
c      initialize
c
       iseed(1)=m
       iseed(2)=m/3
       iseed(3)=m*3
       iseed(4)=2*m+1
c
       do i=1,m
        optModZ(i)=zlarnd(4,iseed)
       end do
c
       return
       end
c    -------------------------------------------------------
      subroutine timeevo(t,optmod,optres)
!
!     Here compute the time evolution of the best amplified
!     optimal perturbation
!
!     t: 	input, time at which the optimum is computed
!     optmod :	optimal mode
!     optres : 	results at time t
!
c     ----------------------------------------------------
      implicit none
c
      integer m
      include 'trunc.h'
c
      integer i
      complex*16 optmod(1:m)
      complex*16 optres(1:m)
      real*8 t
c
      complex*16  ev
      common/aev/ev(1:m)
c
c     -----------------------------------------------------

      do i=1,m
       optres(i)=zexp(ev(i)*t)*optmod(i)
      end do

c     -----------------------------------------------------
      return
      end
c     ********************************************************
       subroutine soutoptmodevo(optmodT,j)
c       
       implicit none
c
       integer m,iout       
       include 'floq3D.h'
       include 'trunc.h'
c
       integer nn
       parameter(nn=3*(n+1))
c
       integer i,k,j
       complex*16 optModT(1:m)
       complex*16 optMod(1:nn)
       complex*16  vr
       common/avr/vr(1:n,1:m)
       real*8 eta(0:n)
c
c       
!      build back mode
       do i=1,nn
        optMod(i)=(0.0d0,0.0d0)
        do k=1,m
         optMod(i)=optMod(i)+vr(i,k)*optModT(k)
        end do
       end do
!      
c
!     read geometry
      open(unit=21,file='eta.dat')
      rewind(21)
      do i=0,n
       read(21,*) eta(i)
      end do 
      close(21) 

c      if (j.eq.0) then
c       open(unit=13,file='optRest0.dat')
c       rewind(13)
c       do i=0,Ny
c         write(13,999) eta(i)
c     & ,dble(optMod(i+1+0*(Ny+1))),dimag(optMod(i+1+0*(Ny+1)))
c     & ,dble(optMod(i+1+1*(Ny+1))),dimag(optMod(i+1+1*(Ny+1)))
c     & ,dble(optMod(i+1+2*(Ny+1))),dimag(optMod(i+1+2*(Ny+1)))
c       end do
c      close(13)
c      end if
!

      do i=0,n
       write(j+20,999) eta(i)
     & ,dble(optMod(i+1+0*(Ny+1))),dimag(optMod(i+1+0*(Ny+1)))
     & ,dble(optMod(i+1+1*(Ny+1))),dimag(optMod(i+1+1*(Ny+1)))
     & ,dble(optMod(i+1+2*(Ny+1))),dimag(optMod(i+1+2*(Ny+1)))
       end do
c       close(23)
c
!
  999  format(30(1X,E16.10))
!
       return
       end
c     #################################################################
      include 'randgen.f'
      include 'support.f'
c     #################################################################

