% $Id: GenDmat.m,v 1.3 2008/04/10 21:05:55 carlo Exp $
% $Source: /moogwai/usr/carlo/bubu/WORK/VALIDATION/MCODE/RCS/GenDmat.m,v $
%%%% parameters
para2D=load('para2DCHA.h');
 N= para2D(1)-1; %N=63;%number of collocation points in wall normal direction
boxsize=2.0; % WARNING still to verify if it is OK

%%%% differentiation matrices with homogeneous BC on both walls
[yvecT,DM] = chebdif(N+2,2);
yvec=yvecT(2:end-1);
% implement homogeneous boundary conditions 
D1=DM(2:N+1,2:N+1,1);
D2=DM(2:N+1,2:N+1,2);
[yvecT,DM] = chebdif(N+2,3);
D3=DM(2:N+1,2:N+1,3);
%%%%%%D3=D1*D2;
% fourth derivative with clamped conditions
[y,D4]=cheb4c(N+2);

    outfil=fopen('D1.dat','w');
     for i=1:N;
      fprintf(outfil,'%1.16E ',D1(i,:));
      fprintf(outfil,'\n');
     end
    fclose(outfil);
%
    outfil=fopen('D2.dat','w');
     for i=1:N;
      fprintf(outfil,'%1.16E ',D2(i,:));
      fprintf(outfil,'\n');
     end
    fclose(outfil);
%
    outfil=fopen('D3.dat','w');
     for i=1:N;
      fprintf(outfil,'%1.16E ',D3(i,:));
      fprintf(outfil,'\n');
     end
    fclose(outfil);
%
    outfil=fopen('D4.dat','w');
     for i=1:N;
      fprintf(outfil,'%1.16E ',D4(i,:));
      fprintf(outfil,'\n');
     end
    fclose(outfil);

quit
